module go-a8-node

go 1.12

require (
	github.com/fd/go-nat v1.0.0 // indirect
	github.com/gogo/protobuf v1.2.1
	github.com/google/uuid v1.1.1
	github.com/libp2p/go-libp2p v0.0.20
	github.com/libp2p/go-libp2p-crypto v0.0.1
	github.com/libp2p/go-libp2p-discovery v0.0.2
	github.com/libp2p/go-libp2p-host v0.0.1
	github.com/libp2p/go-libp2p-kad-dht v0.0.6
	github.com/libp2p/go-libp2p-net v0.0.2
	github.com/libp2p/go-libp2p-peer v0.0.1
	github.com/libp2p/go-libp2p-peerstore v0.0.5
	github.com/libp2p/go-libp2p-protocol v0.0.1
	github.com/mattn/go-sqlite3 v1.10.0
	github.com/multiformats/go-multiaddr v0.0.2
	github.com/sirupsen/logrus v1.4.1
	github.com/urfave/cli v1.20.0
	gitlab.com/autom8.network/go-a8-fn v0.0.0-20190624212249-91a8842a23a5
	gitlab.com/autom8.network/go-a8-http v0.0.0-20190624212122-be32429cde9b
	gitlab.com/autom8.network/go-a8-node v0.0.0-20190329233413-a8df1ea07b97 // indirect
	gitlab.com/autom8.network/go-a8-util v0.0.0-20190624211909-8b2090c44ebe
)

// replace gitlab.com/autom8.network/go-a8-util => ../go-a8-util

// replace gitlab.com/autom8.network/go-a8-http => ../go-a8-http

// replace gitlab.com/autom8.network/go-a8-fn => ../go-a8-fn
