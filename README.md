a8 node in go

How to run this:
1. Have Go.
2. Have bootstrap node running
3. Run `go mod download` (only need to do this once if things change)
4. Run `go build -o a8node`
5. Run `./a8node --http_port 3036 gateway`
6. In a new window run `./a8node --libp2p_port 4002 --http_port 3035`
7. a8cli will now work
