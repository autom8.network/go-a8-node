package main

import (
	"os"
	"strings"
	"time"

	"github.com/urfave/cli"

	a8util "gitlab.com/autom8.network/go-a8-util"
	"go-a8-node/node"
)

var httpPort string
var libp2pPort int64
var bootstrappersRaw string
var bindIP string
var bcastIP string
var fnDomain string
var fnPort int
var idLock int
var bTimeout int

func main() {
	app := cli.NewApp()

	a8util.SetLogLevel(a8util.Trace)

	app.Version = "0.2.3"

	app.Name = "a8node"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "bootstrappers,b",
			Value:       "/ip4/127.0.0.1/tcp/4001/ipfs/QmPMvCo7Y6zKuW3rFekF4S471igJJd4nrErA43y9A3MPfU",
			Usage:       "bootstrap multiadders",
			Destination: &bootstrappersRaw,
		},
		cli.StringFlag{
			Name:        "http-port,t",
			Value:       "3000",
			Usage:       "port webserver will open on",
			Destination: &httpPort,
		},
		cli.Int64Flag{
			Name:        "libp2p-port,l",
			Value:       4000,
			Usage:       "port libp2p will open on",
			Destination: &libp2pPort,
		},
		cli.StringFlag{
			Name:        "bindIP,i",
			Value:       "0.0.0.0",
			Usage:       "p2p bind IP adderess (how you will talk to others)",
			Destination: &bindIP,
		},
		cli.StringFlag{
			Name:        "broadcastIP,c",
			Value:       "0.0.0.0",
			Usage:       "p2p broadcast IP adderess (how others will talk to you)",
			Destination: &bcastIP,
		},
		cli.StringFlag{
			Name:        "fn-dns,f",
			Value:       "localhost",
			Usage:       "fnserver domain name name or ip address",
			Destination: &fnDomain,
		},
		cli.IntFlag{
			Name:        "fn-port,p",
			Value:       80,
			Usage:       "fnserver domain name name or ip address",
			Destination: &fnPort,
		},
		cli.IntFlag{
			Name:        "idLock,d",
			Usage:       "locks ipfs id to port address, only for debugging",
			Destination: &idLock,
		},
		cli.IntFlag{
			Name:        "bootstrap-timeout",
			Usage:       "delay bootstrap node uses",
			Value:       10,
			Destination: &bTimeout,
		},
	}

	app.Action = func(c *cli.Context) {
		nodeType := node.DaemonStr
		if c.NArg() > 0 {
			nodeType = c.Args()[0]
		}

		var bootstrappers = strings.Split(bootstrappersRaw, ",")
		timeoutSeconds := time.Duration(bTimeout) * time.Second

		a8util.Log.WithFields(a8util.Fields{
			"nodeType":         nodeType,
			"libp2pPort":       libp2pPort,
			"httpPort":         httpPort,
			"ipAddr":           bindIP,
			"bcastIP":          bcastIP,
			"bootstrappers":    bootstrappers,
			"idLock":           idLock,
			"version":          app.Version,
			"bootstrapTimeout": bTimeout,
		}).Info("Starting Node")

		go node.Node(
			idLock,
			libp2pPort,
			nodeType,
			httpPort,
			bindIP,
			bcastIP,
			bootstrappers,
			fnDomain,
			fnPort,
			timeoutSeconds,
		)

		select {}
	}

	app.Run(os.Args)
}
