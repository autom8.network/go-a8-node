package node

import (
	fnConnect "gitlab.com/autom8.network/go-a8-fn"
	a8util "gitlab.com/autom8.network/go-a8-util"
	"time"
)

// GatewayStr is the command passed in to start a gateway node
var GatewayStr = "gateway"

// DaemonStr is the command passed in to start a daemon
var DaemonStr = "daemon"

// Node is an a8 node
func Node(
	fixedID int,
	libp2pPort int64,
	nodeType string,
	httpPort string,
	bindIP string,
	broadcastIP string,
	bootstrappers []string,
	fnDNS string,
	fnPort int,
	bootstrapTimeout time.Duration,
) {

	libp2pServer, err := NewServer(fixedID, libp2pPort, bindIP, broadcastIP, bootstrappers, bootstrapTimeout)
	if err != nil {
		panic(err)
	}

	libp2pServer.findBootstrappers()

	switch nodeType {
	case GatewayStr:
		fn := fnConnect.Fn{
			Domain:   fnDNS,
			Port:     fnPort,
			Protocol: "http",
		}

		libp2pServer.createExecutionNode(httpPort, []a8Processor{fn.CallFn})
	case DaemonStr:
		libp2pServer.createDaemonNode(httpPort)
	default:
		a8util.Log.Panic("unknown node type selected: ", nodeType)
	}

	select {}
}
