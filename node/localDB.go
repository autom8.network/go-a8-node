package node

import (
	"database/sql"
	"fmt"
	a8util "gitlab.com/autom8.network/go-a8-util"
	"os"
	"path/filepath"

	//import go=sqlite3, database/sql is used as an interface for it
	_ "github.com/mattn/go-sqlite3"
)

var configDir = "/usr/local/a8"

var dbName = "main.db"

type a8DB struct {
	db *sql.DB
}

func setupDb() (*a8DB, error) {
	dbPath, err := a8FolderPath()
	if err != nil {
		return nil, err
	}

	path := filepath.Join(dbPath, dbName)

	database, err := sql.Open("sqlite3", path)
	if err != nil {
		return nil, err
	}

	statement, err := database.Prepare("CREATE TABLE IF NOT EXISTS nonce (id TEXT PRIMARY KEY)")
	if err != nil {
		return nil, err
	}

	statement.Exec()

	return &a8DB{
		db: database,
	}, nil
}

func (n *a8DB) recordNonce(nonce string) error {
	statement, err := n.db.Prepare("INSERT INTO nonce (id) VALUES (?)")
	if err != nil {
		return err
	}

	if _, err := statement.Exec(nonce); err == nil {
		return err
	}

	a8util.Log.Info(fmt.Sprintf("added nonce %s to db", nonce))

	return nil
}

func (n *a8DB) checkNonce(nonce string) error {
	rows, err := n.db.Query("SELECT id FROM nonce WHERE id = ?", nonce)
	if err != nil {
		return err
	}

	exists := false
	for rows.Next() {
		exists = true
	}

	if exists {
		return fmt.Errorf("nonce already exists")
	}

	return nil
}

func (n *a8DB) VerifyNonce(nonce string) error {
	if err := n.checkNonce(nonce); err != nil {
		return err
	}

	if err := n.recordNonce(nonce); err != nil {
		return err
	}

	return nil
}

//this finds the .a8 folder file path, and if it doesn't exist, creates it
func a8FolderPath() (string, error) {
	_, err := os.Stat(configDir)

	if os.IsNotExist(err) {
		if err := os.MkdirAll(configDir, os.ModePerm); err != nil {
			return "", err
		}
	}

	if err != nil {
		return "", err
	}

	return configDir, nil
}
