package node

import (
	"context"
	cyrptoRand "crypto/rand"
	"errors"
	"fmt"
	"github.com/gogo/protobuf/proto"
	"github.com/google/uuid"
	"io"
	mrand "math/rand"
	"sync"
	"time"

	ggio "github.com/gogo/protobuf/io"
	"github.com/libp2p/go-libp2p"
	crypto "github.com/libp2p/go-libp2p-crypto"
	discovery "github.com/libp2p/go-libp2p-discovery"
	host "github.com/libp2p/go-libp2p-host"
	libp2pdht "github.com/libp2p/go-libp2p-kad-dht"
	libp2pNet "github.com/libp2p/go-libp2p-net"
	peerstore "github.com/libp2p/go-libp2p-peerstore"
	protocol "github.com/libp2p/go-libp2p-protocol"
	maddr "github.com/multiformats/go-multiaddr"
	a8HttpServer "gitlab.com/autom8.network/go-a8-http/server"
	a8util "gitlab.com/autom8.network/go-a8-util"
	a8crypto "gitlab.com/autom8.network/go-a8-util/crypto"
	pb "gitlab.com/autom8.network/go-a8-util/proto"
)

// Server is a libp2p server with dedicated a8 methods
type Server struct {
	ctx               context.Context
	cancel            context.CancelFunc
	host              host.Host
	dht               *libp2pdht.IpfsDHT
	routingDiscovery  *discovery.RoutingDiscovery
	bootstrapNodes    []maddr.Multiaddr
	relayNodeAddr     string
	peers             []peerstore.PeerInfo
	pollChan          chan bool
	bootstrapPollChan chan bool
	bootstrapTimeout  time.Duration
	a8Processors      []a8Processor
	db                *a8DB
}

type libp2pDataProcessor func(*pb.A8Message) *pb.A8Message

type a8Processor func(request *pb.A8Request) *pb.A8Response

var errNoPeers = errors.New("no applicable peers known to this node")

var a8protocolID = protocol.ID("/a8/0.1.0")

// NewServer creates a new libp2p server
func NewServer(
	fixedID int,
	libp2pPort int64,
	bindIP string,
	broadcastIP string,
	bootstrappers []string,
	bootstrapTimeout time.Duration,
) (*Server, error) {
	ctx, cancel := context.WithCancel(context.Background())

	prvKey, err := getPrivateKey(fixedID)
	if err != nil {
		return nil, err
	}

	multiaddrString := fmt.Sprintf("/ip4/%s/tcp/%d", bindIP, libp2pPort)

	// 0.0.0.0 will listen on any interface device.
	sourceMultiAddr, _ := maddr.NewMultiaddr(multiaddrString)

	var extMultiAddr maddr.Multiaddr
	if broadcastIP == "" {
		a8util.Log.Warning("External IP not defined, Peers might not be able to resolve this node if behind NAT\n")
	} else {
		extMultiAddr, err = maddr.NewMultiaddr(fmt.Sprintf("/ip4/%s/tcp/%d", broadcastIP, libp2pPort))
		if err != nil {
			a8util.Log.Errorf("Error creating multiaddress: %v\n", err)
			// return nil, err
		}
	}
	addressFactory := func(addrs []maddr.Multiaddr) []maddr.Multiaddr {
		if extMultiAddr != nil {
			addrs = append(addrs, extMultiAddr)
		}
		return addrs
	}

	host, err := libp2p.New(
		ctx,
		libp2p.ListenAddrs(sourceMultiAddr),
		libp2p.Identity(prvKey),
		libp2p.AddrsFactory(addressFactory),
	)

	a8util.Log.WithFields(a8util.Fields{
		"localPeerId": host.ID().Pretty(),
	}).Info("created local host with peer id")

	if err != nil {
		a8util.Log.Panic("libp2p host failed to be created")
	}

	kademliaDHT, err := libp2pdht.New(ctx, host)
	if err != nil {
		a8util.Log.Panic("kademliaDHT failed to initialize")
	}

	routingDiscovery := discovery.NewRoutingDiscovery(kademliaDHT)

	// create an array of bootstrappers
	var bootstrapPeers []maddr.Multiaddr

	for _, bootstrapper := range bootstrappers {
		ma, err := maddr.NewMultiaddr(bootstrapper)
		if err != nil {
			a8util.Log.WithFields(a8util.Fields{
				"bootstrapper": bootstrapper,
			}).Error("Couldn't make multiaddr from bootstrapper string")
		}
		bootstrapPeers = append(bootstrapPeers, ma)
	}

	return &Server{
		ctx:               ctx,
		cancel:            cancel,
		host:              host,
		dht:               kademliaDHT,
		routingDiscovery:  routingDiscovery,
		bootstrapNodes:    bootstrapPeers,
		pollChan:          make(chan bool),
		bootstrapPollChan: make(chan bool),
		bootstrapTimeout:  bootstrapTimeout,
	}, nil
}

func (s *Server) createExecutionNode(httpPort string, a8Processors []a8Processor) {
	s.a8Processors = a8Processors

	db, err := setupDb()
	if err != nil {
		a8util.Log.Fatal("could not find or set up a DB", err)
		return
	}

	s.db = db

	s.advertiseService("execution")
	s.host.SetStreamHandler(a8protocolID, s.a8streamHandler(s.runRemoteRequest))

	executionServer := a8HttpServer.New(s.ctx, httpPort)
	executionServer.AddA8RequestProcessor(s.runLocalRequest)
	executionServer.Start()
}

func (s *Server) createDaemonNode(httpPort string) {
	go s.activePoll("execution")

	s.pollNetwork()

	shutdownCh := make(chan struct{})
	go s.findPeersInterval(shutdownCh, 20*time.Second, "execution")
	go s.refreshNetworkAfterSleep(shutdownCh, "execution")

	defer close(shutdownCh)

	a8util.Log.Info("the number of total added peers are ", len(s.peers))

	daemonServer := a8HttpServer.New(s.ctx, httpPort)
	daemonServer.AddA8RequestProcessor(s.sendRequestOverLibp2p)
	daemonServer.AddTransactionsProcessor(s.getTransactionData)
	daemonServer.Start()
}

func (s *Server) activePoll(service string) {

	a8util.Log.Info("starting activePoll")

	previousTime := time.Now().Add(-5 * time.Second)
	for {
		select {
		case <-s.pollChan:
			//todo there is probably a better way to discard old pollers
			if time.Now().Sub(previousTime) < 100*time.Millisecond {
				a8util.Log.WithFields(a8util.Fields{
					"service":  service,
					"timeDiff": time.Now().Sub(previousTime),
				}).Info("just finished bootstrap")

				continue
			}

			a8util.Log.WithFields(a8util.Fields{
				"service": service,
			}).Info("we're trying to poll for network nodes in activePoll")

			s.findBootstrappers()
			s.makeNetworkPoll(service)

			previousTime = time.Now()
		}
	}
}

func (s *Server) findBootstrappers() {
	var wg sync.WaitGroup
	for _, peerAddr := range s.bootstrapNodes {
		peerinfo, _ := peerstore.InfoFromP2pAddr(peerAddr)

		wg.Add(1)

		go func() {
			defer wg.Done()
			addressLog := a8util.Log.WithFields(a8util.Fields{
				"peerString": peerinfo.ID.Pretty(),
				"multiaddr":  peerinfo.Addrs,
			})

			ctx, cancel := context.WithTimeout(s.ctx, s.bootstrapTimeout)

			defer cancel()

			if err := s.host.Connect(ctx, *peerinfo); err != nil {
				addressLog.Error("Couldn't find a bootstrap node with peer address ", err)
			} else {
				addressLog.Info("Connection established with bootstrap node")
			}
		}()
	}

	wg.Wait()
}

func (s *Server) a8streamHandler(dataProcessor libp2pDataProcessor) libp2pNet.StreamHandler {
	return func(stream libp2pNet.Stream) {
		a8util.Log.WithFields(a8util.Fields{
			"protocol": stream.Protocol(),
		}).Info("Got a new stream!")

		// Create a buffer stream for non blocking read and write.
		r := ggio.NewDelimitedReader(stream, 2000000)
		w := ggio.NewDelimitedWriter(stream)

		var requestMsg pb.A8Message
		if err := r.ReadMsg(&requestMsg); err != nil {
			a8util.Log.Error(fmt.Sprintf("Error: failed to read message: %v", err))
			return
		}

		responseMsg := dataProcessor(&requestMsg)

		if err := w.WriteMsg(responseMsg); err != nil {
			a8util.Log.Error(fmt.Sprintf("Error: failed to write message: %v", err))
			return
		}
	}
}

func (s *Server) findBootstrapInterval(shutdownCh <-chan struct{}, duration time.Duration) {
	ticker := time.NewTicker(duration)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			s.pollBootstrapsNetwork()
		case <-shutdownCh:
			return
		}
	}
}

func (s *Server) findPeersInterval(shutdownCh <-chan struct{}, duration time.Duration, service string) {
	ticker := time.NewTicker(duration)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			s.pollNetwork()
		case <-shutdownCh:
			return
		}
	}
}

func (s *Server) refreshNetworkAfterSleep(shutdownCh <-chan struct{}, service string) {
	previousTime := time.Now()
	ticker := time.NewTicker(1 * time.Second)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			//a8util.Log.Info(fmt.Sprintf(" the current time is %s old time is %s, difference is %d", time.Now().String(), previousTime.String(), time.Now().Sub(previousTime)))
			if time.Now().Sub(previousTime) > 1250*time.Millisecond {
				a8util.Log.Info("There's been too much of a delay, polling the network")

				s.pollBootstrapsNetwork()
				s.pollNetwork()
			}
			previousTime = time.Now()
		case <-shutdownCh:
			return
		}
	}
}

//sendMsgToPeer sends a A8Message message over a libp2p stream and returns the A8Message response
func (s *Server) sendMsgToPeer(ctx context.Context, msg *pb.A8Message, chosenPeer *peerstore.PeerInfo, protocolID protocol.ID) (*pb.A8Message, error) {
	a8util.Log.WithFields(a8util.Fields{
		"peerID":      chosenPeer.ID,
		"localHostId": s.host.ID(),
		"message":     a8util.TruncateString(msg.String(), 1000),
	}).Info("sending message to a peer")

	if chosenPeer.ID == s.host.ID() {
		return nil, fmt.Errorf("peer ID is same as host, %s", s.host.ID())
	}

	stream, err := s.host.NewStream(ctx, chosenPeer.ID, protocolID)
	if err != nil {
		return nil, err
	}

	defer stream.Close()

	r := ggio.NewDelimitedReader(stream, 2000000)
	w := ggio.NewDelimitedWriter(stream)

	if err := w.WriteMsg(msg); err != nil {
		return nil, err
	}

	var responseMsg pb.A8Message
	if err := r.ReadMsg(&responseMsg); err != nil {
		return nil, err
	}

	return &responseMsg, nil
}

func (s *Server) sendRequestOverLibp2p(req *pb.A8Request) (*pb.A8Response, error) {
	peer, err := s.getRequestPeer(req)
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithTimeout(s.ctx, 5*time.Second)
	defer cancel()

	msg := &pb.A8Message{
		CallId:    a8uuid(),
		MessageId: a8uuid(),
		Call: &pb.A8Message_A8Request{
			A8Request: req,
		},
	}

	responseMsg, err := s.sendMsgToPeer(ctx, msg, peer, a8protocolID)
	if err != nil {
		a8util.Log.WithFields(a8util.Fields{
			"PeerId":     peer.ID,
			"msg":        a8util.TruncateString(msg.String(), 1000),
			"protocolId": a8protocolID,
		}).Error("failed to send a message to a peer \n")

		return nil, err
	}

	return responseMsg.GetA8Response(), nil
}

func (s *Server) getTransactionData() (*pb.Transaction, error) {
	chosenPeer, err := s.choosePeer()

	if err == errNoPeers {
		a8util.Log.Info("Couldn't find peers, trying to get new ones \n")

		s.pollNetwork()

		chosenPeer, err = s.choosePeer()
	}

	if err != nil {
		return nil, err
	}

	return &pb.Transaction{
		NodeId: chosenPeer.ID.Pretty(),
	}, nil
}

func (s *Server) runRemoteRequest(msg *pb.A8Message) *pb.A8Message {
	var a8Resp *pb.A8Response
	a8Req := msg.GetA8Request()

	if err := s.verifyRequest(a8Req); err != nil {
		errMsg := fmt.Sprintf("Error verifying request %s", err)

		a8util.Log.WithFields(a8util.Fields{
			"messageId": msg.MessageId,
			"callId":    msg.CallId,
		}).Error(errMsg)

		a8Resp = &pb.A8Response{
			Response: &pb.A8Response_Error{
				Error: &pb.Error{
					Message: errMsg,
				},
			},
		}
	} else {
		a8Resp = s.processMessage(a8Req)
	}

	a8message := &pb.A8Message{
		MessageId: a8uuid(),
		CallId:    msg.CallId,
		Call: &pb.A8Message_A8Response{
			A8Response: a8Resp,
		},
	}

	a8util.Log.WithFields(a8util.Fields{
		"message": a8util.TruncateString(a8message.String(), 1000),
	}).Info("a8 message response")

	return a8message
}

//runLocalRequest is for a node running a8 rquests from local a8 functions, thus no transaction security data is needed.
func (s *Server) runLocalRequest(msg *pb.A8Request) (*pb.A8Response, error) {
	//todo might need to add call/messageIds, especially for logging

	return s.processMessage(msg), nil
}

func (s *Server) processMessage(request *pb.A8Request) *pb.A8Response {

	//right now this uses the first processor that returns non-nil, can add logic here for better selection in the future
	for _, processor := range s.a8Processors {
		//this returns no error, errors are logged locally with the processor and the whole thing is just skipped
		resp := processor(request)
		if resp != nil {
			return resp
		}
	}

	return nil
}

// this function finds a peer that is known to the host and returns a peerID, this is for choosing a peer to send to,
//   currently it is random, but that should change in the future to something that makes sense
//   random is bad, but not too costly as this is not true random and it works for now
func (s *Server) choosePeer() (*peerstore.PeerInfo, error) {
	if len(s.peers) == 0 {
		return nil, errNoPeers
	}

	chosenPeer := s.peers[int(mrand.Intn(len(s.peers)))]

	return &chosenPeer, nil
}

func (s *Server) pollNetwork() {
	a8util.Log.Info("trying to poll the network")
	s.pollChan <- true
}

func (s *Server) pollBootstrapsNetwork() {
	a8util.Log.Info("trying to poll the network for bootstraps")
	s.bootstrapPollChan <- true
}

// this function looks on the network for a given service being advertised and replaces all existing peers with what it finds
func (s *Server) makeNetworkPoll(service string) {
	currentTime := time.Now()

	a8util.Log.WithFields(a8util.Fields{
		"startTime": currentTime,
	}).Info("looking for peers using routingDiscovery")

	peerChannel, err := s.routingDiscovery.FindPeers(s.ctx, service)
	if err != nil {
		a8util.Log.Error("couldn't run routing discovery ", err)
	}

	var newPeers []peerstore.PeerInfo

	expectedPeers := 0
	for foundPeer := range peerChannel {

		expectedPeers++
		a8util.Log.WithFields(a8util.Fields{
			"peer":       foundPeer,
			"addrsCount": len(foundPeer.Addrs),
		}).Info("Found peer")

		//todo better filtering for what is a good peer,
		//  potentially try to stream a ping to it, and if it works only then add it?
		if len(foundPeer.Addrs) == 0 {
			a8util.Log.Info("no addrs found")
			continue
		}

		a8util.Log.Info("appending new peers")

		newPeers = append(newPeers, foundPeer)

		if expectedPeers >= 2 {
			break
		}
	}

	a8util.Log.WithFields(a8util.Fields{
		"endTime":       time.Now(),
		"ecplipsedTime": time.Now().Sub(currentTime),
		//"peerChannelLength" : len(peerChannel),
	}).Info("finished routingDiscovery")

	a8util.Log.Info("the number of new peers are ", len(newPeers))

	if len(newPeers) == 0 {
		time.Sleep(1 * time.Second)

		go s.pollNetwork()
		return
	}

	s.peers = newPeers

	a8util.Log.Info("the number of added peers are ", len(s.peers))
}

func (s *Server) advertiseService(serviceName string) {
	discovery.Advertise(s.ctx, s.routingDiscovery, serviceName)

	a8util.Log.WithFields(a8util.Fields{
		"rendezvous": "execution",
	}).Info("Successfully announced!")
}

func getPrivateKey(fixedID int) (crypto.PrivKey, error) {
	// If debug is enabled, use a constant random source to generate the peer ID. Only useful for debugging,
	// off by default. Otherwise, it uses rand.Reader.
	var r io.Reader

	if fixedID != 0 {
		// uses fixedID as randomization for private key
		r = mrand.New(mrand.NewSource(int64(fixedID)))
	} else {
		r = cyrptoRand.Reader
	}

	// Creates a new RSA key pair for this host.
	prvKey, _, err := crypto.GenerateKeyPairWithReader(crypto.RSA, 2048, r)
	if err != nil {
		return nil, err
	}

	return prvKey, nil
}

func (s *Server) getRequestPeer(message *pb.A8Request) (*peerstore.PeerInfo, error) {
	peerID := message.Transaction.NodeId

	for _, peer := range s.peers {
		if peer.ID.Pretty() == peerID {
			return &peer, nil
		}
	}

	return nil, fmt.Errorf("no peer with peerID %s found", peerID)
}

//verifyRequest verifies requests have a nonce that hasn't been used and that they are signed properly. Errors from this function are sent back to user
func (s *Server) verifyRequest(a8Req *pb.A8Request) error {
	nonce := a8Req.Transaction.Nonce

	if err := s.db.VerifyNonce(nonce); err != nil {
		a8util.Log.WithFields(a8util.Fields{
			"nonce": nonce,
			"pub":   a8Req.Transaction.PublicKey,
		}).Error(fmt.Sprintf("nonce could not be verified %s", err))
		return fmt.Errorf("nonce could not be verified")
	}

	signature := a8Req.GetSignature()

	//set signature to nil
	a8Req.Signature = nil

	msg, err := proto.Marshal(a8Req)
	if err != nil {
		return err
	}

	//todo see about getting pub from signature
	if a8crypto.VerifyMessage(a8Req.Transaction.PublicKey, msg, signature) {
		err = nil
	} else {
		err = fmt.Errorf("not able to verify signature with message")
	}

	a8Req.Signature = signature
	return err
}

func a8uuid() string {
	return uuid.Must(uuid.NewRandom()).String()
}
