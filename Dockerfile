# Base build image
FROM golang:1.11-alpine AS build-base

# Install some dependencies needed to build the project
RUN apk add bash git gcc g++ libc-dev
ENV D=/go/src/gitlab.com/autom8.network/
WORKDIR $D

# Force the go compiler to use modules
ENV GO111MODULE=on
ENV CGO_ENABLED=1


# We want to populate the module cache based on the go.{mod,sum} files.
COPY go.mod .
COPY go.sum .

# This is the ‘magic’ step that will download all the dependencies that are specified in 
# the go.mod and go.sum file.
# Because of how the layer caching system works in Docker, the  go mod download 
# command will _ only_ be re-run when the go.mod or go.sum file change 
# (or when we add another docker instruction this line)
RUN go mod download

# This image builds the weavaite server
FROM build-base AS node-builder

RUN apk --no-cache add ca-certificates
WORKDIR /go/src/gitlab.com/autom8.network/go-a8-node
# Here we copy the rest of the source code
COPY . . 

# And compile the project
RUN CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go build -o a8-node ./app

#In this last stage, we start from a fresh Alpine image, to reduce the image size and not ship the Go compiler in our production artifacts.
FROM alpine AS a8-node 

EXPOSE 4002
EXPOSE 3036
EXPOSE 3035

# Finally we copy the statically compiled Go binary.
COPY --from=node-builder /go/src/gitlab.com/autom8.network/go-a8-node/a8-node /bin/a8-node
COPY --from=node-builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/


CMD ["a8-node", "--help"]
